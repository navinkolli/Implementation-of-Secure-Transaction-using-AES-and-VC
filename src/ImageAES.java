
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author navin
 */
public class ImageAES {
    
        static String IV = "AAAAAAAAAAAAAAAA";
        static String encryptionKey = "0123456789abcdef";
        private static final int RGB_SIZE = 3;
        private static final int BSHIFT = 0xFF;

static public void main(String args[]) throws Exception {
    try {
        BufferedImage image;
        int width;
        int height;

        File input = new File("C:\\Users\\navin\\Downloads\\QRCode.png");
        image = ImageIO.read(input);
        width = image.getWidth();
        height = image.getHeight();

        byte[] t = new byte[width * height * 3];
        int k = 0;
        int kk = 0;
        int index = 0;

        // fill the table t with RGB values;
        for (int i = 0; i < height; i++) {

            for (int j = 0; j < width; j++) {

                Color c = new Color(image.getRGB(j, i));
                byte r = (byte) c.getRed();
                byte g = (byte) c.getGreen();
                byte b = (byte) c.getBlue();

          t[index++] = r;
          t[index++] = g;
          t[index++] = b;

            }
        }

        // convert table of RGB values into byte Array for the Encryption
        //byte[] bb = integersToBytes(t);

        /* AES Encryption */
        byte[] cipher = encrypt(t, encryptionKey);
        
        t=cipher;

        //t = convertByte2Int(cipher);

        // create image with table RGB values;
      BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
      index = 0;
      for (int i = 0; i < height; i++) {

        for (int j = 0; j < width; j++) {

          // Need to deal with values < 0 so binary AND with 0xFF
          // Java 8 provides Byte.toUnsignedInt but I am from the old school ;-)
          int r = t[index++] & BSHIFT;
          int g = t[index++] & BSHIFT;
          int b = t[index++] & BSHIFT;

          Color newColor = new Color(r, g, b);
          newImage.setRGB(j, i, newColor.getRGB());

        }
      }
        //write the output image
        File ouptut = new File("C:\\Users\\navin\\Documents\\NetBeansProjects\\FinalProject\\created.jpg");
        ImageIO.write(newImage, "jpg", ouptut);

    } catch (Exception e) {
    }
}
    
    
    public static byte[] encrypt(byte[] plainText, String encryptionKey) throws Exception {
    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
    cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV.getBytes("UTF-8")));
    return cipher.doFinal(plainText);
}

public static byte[] integersToBytes(int[] values) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    DataOutputStream dos = new DataOutputStream(baos);
    for (int i = 0; i < values.length; ++i) {
        dos.writeInt(values[i]);
    }

    return baos.toByteArray();
}

public static int[] convertByte2Int(byte buf[]) {
    int intArr[] = new int[buf.length / 4];
    int offset = 0;
    for (int i = 0; i < intArr.length; i++) {
        intArr[i] = (buf[3 + offset] & 0xFF) | ((buf[2 + offset] & 0xFF) << 8) | ((buf[1 + offset] & 0xFF) << 16)
                | ((buf[0 + offset] & 0xFF) << 24);
        offset += 4;
    }
    return intArr;
  }
}
